---
# An instance of the Featurette widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: featurette

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 30

title: 🧠 Skills
subtitle:

# Showcase personal skills or business features.
# - Add/remove as many `feature` blocks below as you like.
# - For available icons, see: https://wowchemy.com/docs/page-builder/#icons
feature:
- description: Python, MATLAB, R, Javascript, C, HTML, CSS, MySQL, Markdown, LaTeX, Bash/zsh
  icon: file-code
  icon_pack: far
  name: Computer Languages
- description: Hybrid modelling, flux analysis, genome-scale modelling, constraint-based modelling, dynamic modellin
  icon: project-diagram
  icon_pack: fas
  name: Systems Biology
- description: Microbiological culture, Animal cell culture (HEK293, U2OS), Molecular cloning, PCR, DNA/RNA extraction, Electrophoresis, qPCR (beginner), BSL-2 laboratory practices, Bioinformatics tools
  icon: dna
  icon_pack: fas
  name: Molecular Biology
- description: Object-oriented programming, Artificial Intelligence, Deep Learning, Artificial Neural Networks, Networking, Web development, Git, Graphics software, *nix command line, Windows, macOS, GNU/Linux
  icon: desktop
  icon_pack: fas
  name: Computing

# Uncomment to use emoji icons.
#- icon: ":smile:"
#  icon_pack: "emoji"
#  name: "Emojiness"
#  description: "100%"  

# Uncomment to use custom SVG icons.
# Place your custom SVG icon in `assets/media/icons/`.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
# For example, reference `assets/media/icons/xyz.svg` as `icon: 'xyz'`
#- icon: "your-custom-icon-name"
#  icon_pack: "custom"
#  name: "Surfing"
#  description: "90%"
---
